package esoft;

public class DecidirCandidaturasCTRL {
    
    public Evento getEventosDoFAE(Utilizador u){
        return CentroEventos.getEventosDoFae(u);
    }
    public Evento selecionaEvento(Evento e){
        return e;
    }
    public Candidatura getInfoCandidatura(Candidatura c){
        return c;
    }
    public Decisao setDecisao(String decisao, String texto){
        Decisao d = new Decisao();
        d.setDecisao(decisao);
        d.setTextoDescritivo(texto);
        return d;
    }
    public void registaDecisao(){}
    
    
}
