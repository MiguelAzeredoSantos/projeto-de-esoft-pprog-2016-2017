package esoft;

import java.util.ArrayList;

public class CentroEventos {

    private static ArrayList<Evento> registoEventos = new ArrayList<>();
    private static ArrayList<Exposicao> registoExposicoes = new ArrayList<>();
    private static ArrayList<Congresso> registoCongressos = new ArrayList<>();
    private static ArrayList<Utilizador> m_utilizadores = new ArrayList<>();
    private static ArrayList<Utilizador> m_organizadores = new ArrayList<>();
    private static ArrayList<Utilizador> m_gestor_eventos = new ArrayList<>();

    CentroEventos() {

    }

    public Exposicao novaExposição() {
        return new Exposicao();
    }

    public boolean validaEvento(Evento e) {
        return true;
    }

    public boolean registaEvento(Evento e) {
        return true;
    }

    private void addEvento(Evento evento) {
        registoEventos.add(evento);
    }

    private void addExposicao(Exposicao exposicao) {
        registoExposicoes.add(exposicao);
    }

    private void addCongresso(Congresso congresso) {
        registoCongressos.add(congresso);
    }

    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    public boolean registaUtilizador(Utilizador u) {
        return true;
    }

    public boolean validaUtilizador(Utilizador u) {
        return true;
    }

    private void addUtilizador(Utilizador utilizador) {
        m_utilizadores.add(utilizador);
    }

    public ArrayList<Utilizador> getUtilizadores() {
        return m_utilizadores;
    }

    public ArrayList<Utilizador> getOrganizadores() {
        return m_organizadores;
    }

    public ArrayList<Evento> getEventos() {
        return registoEventos;
    }

    public void getListaEventosOrganizador() {

    }

    public static Evento getEventosDoFae(Utilizador u) {
        return registoEventos.get(0);

    }
    public void getUtilizador(String id, String lu){
        
    }

    public void getMecanismosAtribuicao(){
            
    }
    
    public boolean validarGestorEventos(){
        return true;
    }
    
    public void addGestorEventos(){
        
    }
    
    public void setDados(/*Dados*/){
        
    }
    public void getListaAreaPericia(){
    }
    
    public void novoPerito(){}
    public void setDados(Perito clone, Perito p){}
    public void removePerito(Perito p){}
    public void adicionaPerito(Perito p){}
}
