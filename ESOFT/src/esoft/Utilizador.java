/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esoft;

/**
 *
 * @author playt_000
 */
public class Utilizador {
    private String nome;
    private String email;
    private String username;
    private String password;
    
    public Utilizador(String nome,String email,String username,String password){
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.password = password;
    }
    public Utilizador(){
        this.nome = "";
        this.email = "";
        this.username = "";
        this.password = "";
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public boolean valida(){
        return true;
    }
    
}
