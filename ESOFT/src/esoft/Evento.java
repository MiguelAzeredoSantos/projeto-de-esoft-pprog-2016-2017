package esoft;

import java.util.ArrayList;

public class Evento {

    private String m_strTitulo;
    private String m_strDescricao;
    private String m_strDataInicio;
    private String m_strDataFim;
    private String m_strLocal;

    private ArrayList<Utilizador> m_utilizadores = new ArrayList<>();
    private ArrayList<Utilizador> m_organizadores = new ArrayList<>();
    private ArrayList<Utilizador> m_FAEs = new ArrayList<>();
    private ArrayList<Candidatura> m_candidaturas = new ArrayList<>();

    public Evento(String titulo, String descricao, String dataInicio, String dataFim, String local) {
        this.m_strTitulo = titulo;
        this.m_strDescricao = descricao;
        this.m_strDataInicio = dataInicio;
        this.m_strDataFim = dataFim;
        this.m_strLocal = local;
    }

    public Evento() {
        this.m_strTitulo = "";
        this.m_strDescricao = "";
        this.m_strDataInicio = "";
        this.m_strDataFim = "";
        this.m_strLocal = "";
    }

    public void setTitulo(String titulo) {
        this.m_strTitulo = titulo;
    }

    public void setDescricao(String descricao) {
        this.m_strDescricao = descricao;
    }

    public void setData(String dataInicio, String dataFim) {
        this.m_strDataInicio = dataInicio;
        this.m_strDataFim = dataFim;
    }

    public void setLocal(String local) {
        this.m_strLocal = local;
    }

    public void addUtilizador(Utilizador utilizador) {
        m_utilizadores.add(utilizador);
    }

    private void addOrganizador(Utilizador utilizador) {
        m_organizadores.add(utilizador);
    }

    public void addFAE(Utilizador utilizador) {
        m_FAEs.add(utilizador);
    }

    public boolean validaEvento() {
        return true;
    }

    public void validaOrganizador(Utilizador utilizador) {
    }

    public boolean valida() {
        return true;
    }

    public ArrayList<Candidatura> getListaCandidaturas() {
        return m_candidaturas;
    }

    public ArrayList<Utilizador> getUtilizadores() {
        return m_utilizadores;
    }

    public ArrayList<Utilizador> getListaFAEs() {
        return m_FAEs;
    }

    public ArrayList<Candidatura> getCandidaturasPorDecidir() {
        return m_candidaturas;
    }

    public ArrayList<Candidatura> getInfoCandidatura() {
        return m_candidaturas;
    }

    public boolean registaDecisao() {
        return true;
    }

    public boolean validaDecisao() {
        return true;
    }

    public boolean registaAssociacoes() {
        return true;
    }

    public void novaAtribuicao(Candidatura c, FAE fae) {

    }
    
    public void setAtribuicoes(){
        
    }
    
    public void copiarCandidatura(Candidatura c){}
    public void setDadosCandidatura(Candidatura c){}
    
    public void novaWorkshop(){
        
    }
    
    public void getListaCongressos(){
        
    }
    
    
            
}

